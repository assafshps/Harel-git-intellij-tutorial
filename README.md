Please follow the insturctions:
1.  Open Intellij, in the welcome screen, select "Get from version control"
2.  The version controll is Git, and the url is https://gitlab.com/assafshps/harel-git-intellij-tutorial.git
3.  Click the "Clone" button, this will clone the repo (like using ```git clone```)
4.  Open the project in Intellij
5.  In the bar below, locate the current git branch you are curently on, which ? 
6.  Create new branch, named: "{name}/feature-1" using the right down bar, checkout to this branch
7.  Under the src directory add two new Java files, java classes
8.  Add the two new files you created to git (from the IDE)
9.  Commit the two new files with an appropriate commit message
10.  Try to run the code (if needed add application configuration with the main class main)
11.  You should see the "Welcome to git tutorial" message in the console
12.  Go to the main class, add the line: ```System.out.println("Welcome {name}");``` before the "welcome to git tutorial" message, where name= your name
13.  Go to the main class, add the line: ```System.out.println("Bye {name}");``` after the "welcome to git tutorial" message
14.  Run the application and ensure it is working and all three messages are written in the console
15.  Open the commit dialo, select to commit and push
16.  Wait for the push message completion
17.  Go to gitlab and make sure your new branch is listed in the branches list 
18.  In Gitlab, select to create new merge request on your branch, to master
19.  Make sure that you want to metrge your changes (your branch) to the master branch
20.  Review the commits and changes and ensure it is the changes you did
21.  Click on create merge request
22.  Select "Merge Requests" from the left bar, make sure your merge request is listed, click on it
23.  Now, wait for your friends for other merge request to be listed

After Merge Requests
==========
1.  Select one of your friends merge request, review it, comment on it, look at the changes
2.  At the end of your review, select to merge it (select to delete source branch and to squash commits before clicking merge), click merge
3.  Return to intellij, checkout to the master branch
4.  Make sure you don't see the two new files you added in the feature branch you created
5.  From the upper git menu, select "Update project"
6.  Select "Merge incoming changes into the current branch"
7.  Now you should see your files updated acordinly
8.  In the "Version control" tab, select the "console" tab, see which command was run ? was it ```git pull```
